# frozen_string_literal: true

module VideoPlayer
  class Configuration < WidgetInstanceConfiguration
      attribute :video_url, :string, default: ""
      attribute :loop, :boolean, default: false

      validates :loop, boolean: true
  end
end
