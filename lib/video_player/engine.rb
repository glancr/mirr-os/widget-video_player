# frozen_string_literal: true

module VideoPlayer
  class Engine < ::Rails::Engine
    isolate_namespace VideoPlayer
    config.generators.api_only = true
  end
end
