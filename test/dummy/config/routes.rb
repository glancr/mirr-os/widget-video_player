# frozen_string_literal: true

Rails.application.routes.draw do
  mount VideoPlayer::Engine => '/video_player'
end
