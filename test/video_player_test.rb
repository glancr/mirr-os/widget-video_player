# frozen_string_literal: true

require 'test_helper'

class VideoPlayer::Test < ActiveSupport::TestCase
  test 'truth' do
    assert_kind_of Module, VideoPlayer
  end
end
