# frozen_string_literal: true

require 'json'
$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'video_player/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'video_player'
  s.version     = VideoPlayer::VERSION
  s.authors     = ['Tobias Grasse']
  s.email       = ['tg@glancr.de']
  s.homepage    = 'https://glancr.de/modules/video_player'
  s.summary     = 'mirr.OS widget that plays videos and supports YouTube links.'
  s.description = 'Plays videos. Supports YouTube links.'
  s.license     = 'MIT'
  s.metadata    = { 'json' =>
                    {
                      type: 'widgets',
                      title: {
                        enGb: 'Video Player',
                        deDe: 'Videoplayer',
                        frFr: 'Lecteur Vidéo',
                        esEs: 'Reproductor de Video',
                        plPl: 'Odtwarzacz wideo',
                        koKr: '비디오 플레이어'
                      },
                      description: {
                        enGb: s.description,
                        deDe: 'Spielt Videos ab. Unterstützt YouTube-Links.',
                        frFr: 'Lit des vidéos. Prend en charge les liens YouTube.',
                        esEs: 'Reproduce videos. Soporta enlaces de YouTube.',
                        plPl: 'Odtwarza filmy. Obsługuje linki YouTube.',
                        koKr: '동영상을 재생합니다. YouTube 링크를 지원합니다.'
                      },
                      sizes: [
                        {
                          w: 8,
                          h: 5
                        },
                        {
                          w: 4,
                          h: 3
                        }
                      ],
                      languages: %i[enGb deDe frFr esEs plPl koKr],
                      group: nil
                    }.to_json }

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  s.add_development_dependency 'rails'
  s.add_development_dependency 'rubocop', '~> 0.81'
  s.add_development_dependency 'rubocop-rails'
end
